import math
class Punto():
    x = int
    y = int
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def __str__(self):
        return "({}, {})".format(self.x, self.y)
    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("A = {} Pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print(" B = {} pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("C = {} pertenece al tercer cuadrante".format(self))
        elif self.x > 0 and self.y < 0:
            print("D= {} pertenece al cuarto cudrante").format(self)
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("D = {} se encuentra sobre el origen \n ".format(self))

    def vector(self, p):
            self, p, p.x - self.x, p.y - self.y

    def distancia(self, p):
        d = math.sqrt((p.x - self.x) ** 2 + (p.y - self.y) ** 2)
        print("La distancia entre los puntos {} y {} es {}".format(self, p, d))

class Rectangulo:

    def __init__(self, punto_inicial=Punto(), punto_final=Punto()):
        self.pI = punto_inicial
        self.pF = punto_final

        self.Base = abs(self.pF.x - self.pI.x)
        self.Altura = abs(self.pF.y - self.pI.y)
        self.Area = self.Base * self.Altura

    def base(self):
        print("\n Base del rectangulo= {} ".format(self.Base))

    def altura(self):
        print("\n Altura del rectangulo= {} ".format(self.Altura))

    def area(self):
        print(" Area del rectangulo es= {} ".format(self.Area))

A = Punto(2, 3)
B = Punto(5, 5)
C = Punto(-3, -1)
D = Punto(0, 0)
print(f'A = {A}')
print(f'B = {B}')
print(f'C = {C}')
print(f'D = {D}\n')

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()

